﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> PlayerItems = new List<Item>();
    public ItemDatabase ItemDatabase;

    public void GiveItem(int id)
    {
        var itemToAdd = ItemDatabase.GetItem(id);
        PlayerItems.Add(itemToAdd);
        Debug.Log("Gave Player Item: " + itemToAdd.Title);
    }

    public void GiveItem(string title)
    {
        var itemToAdd = ItemDatabase.GetItem(title);
        PlayerItems.Add(itemToAdd);
        Debug.Log("Gave Player Item: " + itemToAdd.Title);
    }

    public Item CheckForItem(int id)
    {
        return PlayerItems.Find(item => item.Id == id);
    }

    public void RemoveItem(int id)
    {
        Item item = CheckForItem(id);
        if (item != null)
        {
            PlayerItems.Remove(item);
            Debug.Log("Player Item Removed: " + item.Title);
        }
    }
}
