﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    public List<Item> Items = new List<Item>();

    public void BuildDatabase()
    {
        Items = new List<Item>
        {
            new Item(1, "Arms", "Player can now grab ledges and climb over them") {ItemAttributes = new Dictionary<string, int>{{ "Arms", 1 }}},
            new Item(2, "Legs", "Player can now walk smoothly, and not have to bounce to move around.") {ItemAttributes = new Dictionary<string, int>{{ "Legs", 1 }}},
            new Item(3, "Flashlight", "Player has the ability to see paths that were not visible to them before."),
            new Item(4, "Machete", "Player can cut through heavy brush to make a path.")
        };
    }

    void Awake()
    {
        BuildDatabase();
    }

    public Item GetItem(int id)
    {
        return Items.Find(item => item.Id == id);
    }

    public Item GetItem(string title)
    {
        return Items.Find(item => item.Title == title);
    }
}
