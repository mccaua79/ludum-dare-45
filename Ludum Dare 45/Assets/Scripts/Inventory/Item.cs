﻿using System.Collections.Generic;

public class Item
{
    public int Id;
    public string Title;
    public string Description;
    public Dictionary<string, int> ItemAttributes = new Dictionary<string, int>();

    public Item(int id, string title, string description)
    {
        Id = id;
        Title = title;
        Description = description;
    }

    public Item(Item item)
    {
        Id = item.Id;
        Title = item.Title;
        Description = item.Description;
        ItemAttributes = item.ItemAttributes;
    }
}
