﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement")]
    public float Speed;

    [Header("Jump")]
    public float JumpForce;
    public float JumpForceWithLegs;
    public LayerMask GroundLayer;
    public Transform GroundCheck;
    public float GroundCheckRadius = 0.25f;
    public float JumpWaitTime = 0.5f;
    
    private bool _flip = false;
    private bool _jump = false;
    private Rigidbody2D _rb2d;
    private bool _grounded = false;
    private float _nextJump;

    void Start()
    {
        _rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckForGround();
        Move();
    }

    private void Move()
    {
        if (Input.GetKey(KeyCode.A))
        {
            _rb2d.AddForce(new Vector2(-Speed, 0), ForceMode2D.Force);
            _flip = true;
            // move left
        }
        else if (Input.GetKey(KeyCode.D))
        {
            _rb2d.AddForce(new Vector2(Speed, 0), ForceMode2D.Force);
            _flip = false;
        }

        if (Input.GetKey(KeyCode.W) && _grounded && JumpCoolDownCleared())
        {
            _jump = true;
        }
    }

    private bool JumpCoolDownCleared()
    {
        return Time.time > _nextJump;
    }

    void CheckForGround()
    {
        _grounded = Physics2D.OverlapCircle(GroundCheck.position, GroundCheckRadius, GroundLayer);
    }

    void FixedUpdate()
    {
        var localScale = transform.localScale;
        localScale.x = (_flip ? -1 : 1);
        transform.localScale = localScale;

        if (_jump)
        {
            _rb2d.AddForce(new Vector2(0f, JumpForce));
            _nextJump = Time.time + JumpWaitTime;
            _jump = false;
        }
    }
}
