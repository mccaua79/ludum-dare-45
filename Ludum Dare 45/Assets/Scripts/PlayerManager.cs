﻿using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public Inventory PlayerInventory;
    public GameObject GroundCheck;
    public GameObject Legs;
    public GameObject Arms;
    public float LegsGroundCheck = -0.85f;
    public float NoLegsGroundCheck = -0.55f;

    private PlayerMovement PlayerMovement;
    private bool _hasLegs = false;
    private bool _hasArms = false;

    private void Start()
    {
        PlayerMovement = GetComponent<PlayerMovement>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Collectibles")
        {
            var collectible = other.GetComponent<CollectibleItem>();
            if (collectible != null)
            {
                CollectItem(collectible, other.gameObject);
            }
        }
    }

    private void CollectItem(CollectibleItem collectible, GameObject other)
    {
        var itemId = collectible.ItemId;
        PlayerInventory.GiveItem(itemId);
        CheckIfKeyItem(itemId);
        Destroy(other);
    }

    private void CheckIfKeyItem(int itemId)
    {
        var item = PlayerInventory.CheckForItem(itemId);
        if (item.ItemAttributes.ContainsKey("Arms"))
        {
            _hasArms = true;
        }
        else if (item.ItemAttributes.ContainsKey("Legs"))
        {
            _hasLegs = true;
        }
    }

    private void Update()
    {
        if (_hasArms && !Arms.activeSelf)
        {
            Arms.SetActive(true);
        }

        if (_hasLegs && !Legs.activeSelf)
        {
            Legs.SetActive(true);

            // sets the ground check to a new height since legs are added
            var position = GroundCheck.transform.localPosition;
            position.y = LegsGroundCheck;
            GroundCheck.transform.localPosition = position;

            PlayerMovement.JumpForce = PlayerMovement.JumpForceWithLegs;
        }
    }
}
